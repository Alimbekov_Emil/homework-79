const express = require("express");
const cors = require("cors");
const items = require("./app/items");
const category = require("./app/category");
const places = require("./app/places");
const mysqlDb = require("./mysqlDb");

const app = express();

app.use(express.json());
app.use(express.static("public"));
app.use(cors());

const port = 8002;

app.use("/category", category);
app.use("/places", places);
app.use("/items", items);

const run = async () => {
  await mysqlDb.connect();

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
};

run().catch(console.error);
