const express = require("express");
const mysqlDb = require("../mysqlDb");

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const [places] = await mysqlDb.getConnection().query("SELECT id,title FROM places");
    return res.send(places);
  } catch (e) {
    return res.send({ error: e });
  }
});

router.get("/:id", async (req, res) => {
  try {
    const [result] = await mysqlDb
      .getConnection()
      .query("SELECT * FROM places WHERE id = ?", [req.params.id]);
    const places = result[0];

    if (places === undefined) {
      return res.send("There is no such places");
    }

    return res.send(places);
  } catch {
    return res.send({ error: e });
  }
});

router.post("/", async (req, res) => {
  const places = req.body;

  if (places.title) {
    const [result] = await mysqlDb
      .getConnection()
      .query("INSERT INTO places (title,description) VALUES (?,?)", [places.title, places.description]);

    return res.send({ ...places, id: result.insertId });
  }

  return res.status(400).send({ error: "Insufficient data entered" });
});

router.delete("/:id", async (req, res) => {
  try {
    await mysqlDb.getConnection().query("DELETE FROM places WHERE id = ?", [req.params.id]);
    return res.send("Removal was successful");
  } catch (e) {
    return res.status(400).send(e);
  }
});

module.exports = router;
