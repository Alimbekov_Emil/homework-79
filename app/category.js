const express = require("express");
const mysqlDb = require("../mysqlDb");

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const [category] = await mysqlDb.getConnection().query("SELECT id,title FROM category");
    return res.send(category);
  } catch (e) {
    return res.send({ error: e });
  }
});

router.get("/:id", async (req, res) => {
  try {
    const [result] = await mysqlDb.getConnection()
      .query("SELECT * FROM category WHERE id = ?", [req.params.id]);
    const category = result[0];

    if (category === undefined) {
      return res.send("There is no such category");
    }

    return res.send(category);
  } catch (e) {
    return res.send({ error: e });
  }
});

router.post("/", async (req, res) => {
  const category = req.body;

  if (category.title) {
    const [result] = await mysqlDb
      .getConnection()
      .query("INSERT INTO category (title,description) VALUES (?,?)", [category.title, category.description]);

    return res.send({ ...category, id: result.insertId });
  }

  return res.status(400).send({ error: "Insufficient data entered" });
});

router.delete("/:id", async (req, res) => {
  try {
    await mysqlDb.getConnection().query("DELETE FROM category WHERE id = ?", [req.params.id]);
    return res.send("Removal was successful");
  } catch (e) {
    return res.status(400).send(e);
  }
});

module.exports = router;
