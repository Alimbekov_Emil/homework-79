const express = require("express");
const mysqlDb = require("../mysqlDb");
const multer = require("multer");
const nanoid = require("nanoid");
const config = require("../config");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});
const router = express.Router();

const upload = multer({ storage });

router.get("/", async (req, res) => {
  try {
    const [items] = await mysqlDb.getConnection().query("SELECT id,title,category_id,place_id FROM items");
    return res.send(items);
  } catch (e) {
    return res.send({ error: e });
  }
});

router.get("/:id", async (req, res) => {
  try {
    const [result] = await mysqlDb.getConnection().query("SELECT * FROM items WHERE id = ?", [req.params.id]);
    const items = result[0];

    if (items === undefined) {
      return res.send("There is no such items");
    }

    return res.send(items);
  } catch (e) {
    return res.send({ error: e });
  }
});

router.post("/", upload.single("image"), async (req, res) => {
  const items = req.body;

  if (req.file) {
    items.image = req.file.filename;
  }

  items.date = new Date().toISOString();

  if (items.title && items.category_id && items.place_id) {
    const [result] = await mysqlDb
      .getConnection()
      .query("INSERT INTO items (title,category_id,place_id,description,image,date) VALUES (?,?,?,?,?,?)", [
        items.title,
        items.category_id,
        items.place_id,
        items.description,
        items.image,
        items.date,
      ]);

    return res.send({ ...items, id: result.insertId });
  }

  return res.status(400).send({ error: "Insufficient data entered" });
});

router.delete("/:id", async (req, res) => {
  try {
    await mysqlDb.getConnection().query("DELETE FROM items WHERE id = ?", [req.params.id]);
    res.send("Removal was successful");
  } catch (e) {
    return res.send({ error: e });
  }
});

module.exports = router;
