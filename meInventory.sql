create schema Meinventory collate utf8_general_ci;

use Meinventory;

create table category
(
	id int auto_increment,
	title varchar(255) not null,
	description text null,
	constraint category_pk
		primary key (id)
);

create table places
(
	id int auto_increment,
	title varchar(255) not null,
	description text null,
	constraint places_pk
		primary key (id)
);

create table items
(
	id int auto_increment,
	title varchar(255) not null,
	category_id int not null,
	place_id int not null,
	description text null,
	image varchar(45) NULL,
	date text not null,
	constraint items_pk
		primary key (id),
	constraint items_category_id_fk
		foreign key (category_id) references category (id)
			on update cascade,
	constraint items_places_id_fk
		foreign key (place_id) references places (id)
			on update cascade
);


    INSERT INTO category (id, title, description)
    VALUES
    (1, 'Table', 'Some Table'),
    (2, 'Computer', 'Some Computer'),
    (3, 'Appliances', 'Some Appliances');

    INSERT INTO places (id, title, description)
    VALUES
    (100, 'Computer Room', 'Some description'),
    (101, 'Server Room' ,'Some description'),
    (102, 'Kitchen' ,'Some description');

    INSERT INTO items (id, title, category_id, place_id,description, date )
    VALUES
    (10, 'Computer Table', 1, 100, 'Some description','10.10.2020'),
    (11, 'Server Computer', 2, 101, 'Some description','10.10.2021'),
    (12, 'Kitchen Table', 3, 102, 'Some description','10.10.2022')
